from __future__ import annotations

from argparse import ArgumentParser
from functools import partial
from glob import glob
from multiprocessing.pool import ThreadPool
from os import devnull, environ
from shlex import join
from shutil import which
import sys
from subprocess import Popen
from typing import NamedTuple

from pdm.cli.commands.base import BaseCommand
from pdm.cli.commands.run import HookManager
from pdm.project import Project
from rich.console import Console

from pdm_mono.output import RunContext, handle_subprocess
from pdm_mono.progress import create_progress
from pdm_mono.runner import MonoRunner

console = Console(highlight=False)


class MonoSettings(NamedTuple):
    packages: list[str]


class MonoCommand(BaseCommand):
    def add_arguments(self, parser: ArgumentParser):
        parser.add_argument(
            "-c",
            "--concurrent",
            help="Run package scripts in parallel",
            action="store_true",
        )

        parser.add_argument(
            "-q",
            "--quiet",
            help="Suppress output from package scripts",
            action="store_true",
        )

        parser.add_argument(
            "-s",
            "--silent",
            help="Suppress all output",
            action="store_true",
        )

        parser.add_argument("cmd")
        parser.add_argument("args", nargs="*")

    def get_settings(self, project: Project):
        if "mono" not in project.pyproject.settings["plugins"]:
            raise RuntimeError(
                "mono plugin requires list of packages in pyproject.toml"
            )

        return MonoSettings(**project.pyproject.settings["plugins"]["mono"])

    def handle(self, project, options):
        environ["PDM_IGNORE_SAVED_PYTHON"] = "1"
        settings = self.get_settings(project)

        package_projects = [
            Project(project.core, root_path=project.root / resolved, is_global=True)
            for package in settings.packages
            for resolved in glob(package)
        ]

        any_scripts = any(
            options.cmd in package_project.scripts
            for package_project in package_projects
        )

        run_projects: list[tuple[Project, Popen]] = []
        for package_project in package_projects:
            script = package_project.scripts.get(options.cmd, None)
            command = which(options.cmd)

            if (any_scripts and script is None) or (script is None and command is None):
                continue

            run_projects.append(
                package_project,
            )

        if options.quiet or options.silent:
            file = open(devnull, "w")
            console.file = file

        console.print(
            "Scope:",
            len(run_projects),
            "out of",
            len(package_projects),
            "mono projects",
        )

        exit_code = 0
        with create_progress(
            console=console if not options.quiet else None,
            silent=options.silent,
        ) as progress:
            if options.concurrent:
                runs: list[RunContext] = []
                for index, package_project in enumerate(run_projects):
                    hooks = HookManager(package_project)
                    runner = MonoRunner(package_project, hooks)

                    runs.append(
                        RunContext(
                            index=index,
                            project=package_project,
                            subprocess=runner.run(options.cmd, options.args),
                            task=progress.add_task(
                                package_project.root.relative_to(project.root),
                                total=1,
                            ),
                        )
                    )

                with ThreadPool(min(len(runs), 4)) as pool:
                    codes = pool.map(
                        partial(
                            handle_subprocess,
                            progress=progress,
                            project=project,
                            options=options,
                        ),
                        runs,
                    )

                    exit_code = sum(codes)
            else:
                for index, package_project in enumerate(run_projects):
                    hooks = HookManager(package_project)
                    runner = MonoRunner(package_project, hooks)

                    run = RunContext(
                        index=index,
                        project=package_project,
                        subprocess=runner.run(options.cmd, options.args),
                        task=progress.add_task(
                            package_project.root.relative_to(project.root),
                            total=1,
                        ),
                    )

                    exit_code += handle_subprocess(
                        run,
                        progress=progress,
                        project=project,
                        options=options,
                    )

        for package_project in package_projects:
            package_project._saved_python = None

        sys.exit(exit_code)
