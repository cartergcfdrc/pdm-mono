from __future__ import annotations

import os
import signal
import subprocess
import sys
from types import FrameType
from typing import TYPE_CHECKING, Mapping, Sequence

from pdm import termui
from pdm.cli.commands.run import TaskRunner
from pdm.exceptions import PdmUsageError
from pdm.utils import is_path_relative_to

if TYPE_CHECKING:
    from pdm.cli.commands.run import EnvFileOptions


class MonoRunner(TaskRunner):
    def _run_process(
        self,
        args: Sequence[str] | str,
        chdir: bool = False,
        shell: bool = False,
        site_packages: bool = False,
        env: Mapping[str, str] | None = None,
        env_file: EnvFileOptions | str | None = None,
    ) -> subprocess.Popen:
        project = self.project
        process_env = os.environ.copy()
        if env_file is not None:
            if isinstance(env_file, str):
                path = env_file
                override = False
            else:
                path = env_file["override"]
                override = True

            import dotenv

            project.core.ui.echo(
                f"Loading .env file: [success]{env_file}[/]",
                err=True,
                verbosity=termui.Verbosity.DETAIL,
            )
            dotenv_env = dotenv.dotenv_values(project.root / path, encoding="utf-8")
            if override:
                process_env = {**process_env, **dotenv_env}
            else:
                process_env = {**dotenv_env, **process_env}
        pythonpath = process_env.get("PYTHONPATH", "").split(os.pathsep)
        project_env = project.environment
        this_path = project_env.get_paths()["scripts"]
        process_env.update(project_env.process_env)
        if env:
            process_env.update(env)
        if shell:
            assert isinstance(args, str)
            expanded_args: str | Sequence[str] = os.path.expandvars(args)
        else:
            assert isinstance(args, Sequence)
            command, *args = args
            if command.endswith(".py"):
                args = [command, *args]
                command = str(project.environment.interpreter.executable)
            expanded_command = project_env.which(command)
            if not expanded_command:
                raise PdmUsageError(
                    f"Command [success]'{command}'[/] is not found in your PATH."
                )
            expanded_command = os.path.expanduser(os.path.expandvars(expanded_command))
            real_command = os.path.realpath(expanded_command)
            expanded_args = [
                os.path.expandvars(arg) for arg in [expanded_command, *args]
            ]
            if (
                project_env.is_local
                and not site_packages
                and (
                    os.path.basename(real_command).startswith("python")
                    or is_path_relative_to(expanded_command, this_path)
                )
            ):
                # The executable belongs to the local packages directory.
                # Don't load system site-packages
                process_env["NO_SITE_PACKAGES"] = "1"

        cwd = project.root if chdir else None

        def forward_signal(signum: int, frame: FrameType | None) -> None:
            if sys.platform == "win32" and signum == signal.SIGINT:
                signum = signal.CTRL_C_EVENT
            process.send_signal(signum)

        handle_term = signal.signal(signal.SIGTERM, forward_signal)
        handle_int = signal.signal(signal.SIGINT, forward_signal)
        process = subprocess.Popen(
            expanded_args,
            cwd=cwd,
            env=process_env,
            shell=shell,
            bufsize=1,
            universal_newlines=True,
            stdout=subprocess.PIPE,
            stderr=subprocess.PIPE,
        )
        signal.signal(signal.SIGTERM, handle_term)
        signal.signal(signal.SIGINT, handle_int)
        return process
