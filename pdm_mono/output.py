from __future__ import annotations

import selectors
from argparse import Namespace
from functools import partial
from shlex import join
from subprocess import Popen
from typing import NamedTuple

from pdm.project import Project
from rich.console import Console
from rich.progress import Progress, TaskID

COLOR_WHEEL = ["cyan", "magenta", "blue", "yellow", "green", "red"]


class RunContext(NamedTuple):
    index: int
    project: Project
    subprocess: Popen
    task: TaskID


def handle_subprocess(
    run: RunContext,
    *,
    project: Project,
    progress: Progress,
    options: Namespace,
):
    color = COLOR_WHEEL[run.index % len(COLOR_WHEEL)]
    prefix = [
        f"[{color}]{run.project.root.relative_to(project.root)}[/{color}]",
        f"[cyan]{options.cmd}[/cyan]:",
    ]

    progress.console.print(
        f"[{color}]{run.project.root.relative_to(project.root)}[/{color}]",
        f"[cyan]{options.cmd}[/cyan]$",
        join(run.subprocess.args),
    )

    def handle_output(console: Console, fmt: str, stream, _):
        while line := stream.readline():
            console.print(*prefix, fmt.format(line.rstrip()))

    selector = selectors.DefaultSelector()
    selector.register(
        run.subprocess.stdout,
        selectors.EVENT_READ,
        partial(handle_output, progress.console, "{}"),
    )

    selector.register(
        run.subprocess.stderr,
        selectors.EVENT_READ,
        partial(handle_output, progress.console, "[dim]{}[/dim]"),
    )

    while run.subprocess.poll() is None:
        # Wait for events and handle them with their registered callbacks
        events = selector.select()
        for key, mask in events:
            callback = key.data
            callback(key.fileobj, mask)

    exit_code = run.subprocess.wait()
    selector.close()
    progress.update(run.task, advance=1, exit_code=exit_code)

    if run.subprocess.returncode != 0:
        progress.console.print(*prefix, "Failed")

    return exit_code
