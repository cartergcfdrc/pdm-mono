from __future__ import annotations

from pdm.core import Core


from pdm_mono.mono import MonoCommand


def mono_plugin(core: Core):
    core.register_command(MonoCommand, "mono")
