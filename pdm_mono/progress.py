from __future__ import annotations

from rich.console import Console
from rich.progress import Progress, ProgressColumn, SpinnerColumn, Task, TextColumn
from rich.text import Text


class StatusColumn(ProgressColumn):
    def __init__(self):
        super().__init__()
        self.spinner = SpinnerColumn(style="white")
        self.success = Text("✔", style="green")
        self.failure = Text("✘", style="red")

    def render(self, task: Task):
        if task.finished:
            if task.fields["exit_code"] == 0:
                return self.success
            else:
                return self.failure
        return self.spinner.render(task)


class TimeElapsedColumn(ProgressColumn):
    """Renders time elapsed."""

    def render(self, task: Task):
        """Show time remaining."""
        elapsed = task.finished_time if task.finished else task.elapsed
        if elapsed is None:
            return Text("", style="progress.elapsed")
        seconds = elapsed if elapsed < 90 else elapsed % 60
        minutes = elapsed // 60
        return Text(
            str(
                f"{seconds:.1f} s"
                if elapsed < 90
                else f"{minutes:.0f} min {seconds:.0f} s"
            ),
            style="progress.elapsed",
        )


def create_progress(console: Console, silent: bool):
    """Creates a progress instance to be used by the parallel and serial subprocess runners."""
    return Progress(
        StatusColumn(),
        TextColumn("[progress.description]{task.description}"),
        TimeElapsedColumn(),
        console=console,
        disable=silent,
    )
